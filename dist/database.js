"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const config_1 = __importDefault(require("./config/config"));
// Opciones de conexion a la bd
const dbOptions = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    user: config_1.default.DB.USER,
    pass: config_1.default.DB.PASSWORD
};
// Conexion con la bd 
mongoose_1.default.connect(config_1.default.DB.URI, dbOptions);
const connection = mongoose_1.default.connection;
// Informe sobre la conexion 'open'
connection.once('open', () => {
    console.log('conexion establecida con Mongo DB');
});
// Informe sobre la conexion 'error'
connection.once('error', (err) => {
    console.log(err);
    process.exit(0);
});
