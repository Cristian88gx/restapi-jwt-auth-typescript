"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_model_1 = __importDefault(require("../models/user.model"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = __importDefault(require("../config/config"));
// Crear token
function createToken(user) {
    return jsonwebtoken_1.default.sign({ id: user.id, email: user.email }, config_1.default.jwtSecret, {
        expiresIn: 86400 // tiempo de expiracion del token
    });
}
//Registrarse
exports.signUp = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.body.email || !req.body.password) { // Verifico si el usuario me manda email y password
        return res.status(400).json({ msg: 'Debe Ingresar su email y password' });
    }
    const user = yield user_model_1.default.findOne({ email: req.body.email }); // Busco en la bd si ya esta registrado el email
    if (user) {
        return res.status(404).json({ msg: 'El email ya esta registrado' });
    }
    const newUser = new user_model_1.default(req.body); // Creo un nuevo usuario y le asigno el body del request
    yield newUser.save(); // guardo en la bd
    return res.status(400).json(newUser); // devuelvo el status y la info del nuevo usuario
});
// Loguearse
exports.signIn = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.body.email || !req.body.password) { // Verifico si el usuario me manda email y password
        return res.status(400).json({ msg: 'Debe Ingresar su email y password' });
    }
    const user = yield user_model_1.default.findOne({ email: req.body.email }); // Busco en la bd si ya esta registrado el email
    if (!user) {
        return res.status(400).json({ msg: 'El usuario no existe' });
    }
    const isMatch = user.comparePassword(req.body.password); // comparo el password ingresado 
    if (isMatch) {
        return res.status(200).json({ token: createToken(user) }); // devuelvo el token 
    }
    return res.status(400).json({
        msg: 'Email o password incorrecto'
    });
});
