import User from '../models/user.model';
import { Strategy, ExtractJwt, StrategyOptions } from 'passport-jwt';
import config from '../config/config';

// Opciones de la estragia de validacion
const opts: StrategyOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.jwtSecret
};
// Estrategia de validacion
export default new Strategy(opts, async (payload, done) => {
    try {
        const user = await User.findById(payload.id); // busco en la bd por el id del usuario pasado por el payload
        if (user) {
            return done(null, user); // si existe devuelvo el usuario
        }
        return done(null, false); // sino devuelvo false
    } catch (error) {
        console.log(error);
    }
});