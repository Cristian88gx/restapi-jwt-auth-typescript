import mongoose, { ConnectionOptions } from 'mongoose';
import config from './config/config';

// Opciones de conexion a la bd
const dbOptions: ConnectionOptions = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    user: config.DB.USER,
    pass: config.DB.PASSWORD
};

// Conexion con la bd 
mongoose.connect(config.DB.URI, dbOptions);

const connection = mongoose.connection;

// Informe sobre la conexion 'open'
connection.once('open', () => {
    console.log('conexion establecida con Mongo DB');
});

// Informe sobre la conexion 'error'
connection.once('error', (err) => {
    console.log(err);
    process.exit(0);
});



