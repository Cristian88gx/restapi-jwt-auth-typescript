import { Request, Response } from 'express';
import User, { IUser } from '../models/user.model';
import jwt from 'jsonwebtoken';
import config from '../config/config';

// Crear token
function createToken(user: IUser){
    return jwt.sign({id: user.id, email: user.email}, config.jwtSecret,{
        expiresIn: 86400 // tiempo de expiracion del token
    });
}
//Registrarse
export const signUp = async ( req: Request, res: Response ):Promise<Response> => {
    if (!req.body.email || !req.body.password) { // Verifico si el usuario me manda email y password
        return res.status(400).json({msg: 'Debe Ingresar su email y password'});
    }
    const user = await User.findOne({email: req.body.email}); // Busco en la bd si ya esta registrado el email
    if (user) {
        return res.status(404).json({msg: 'El email ya esta registrado'});
    }
    const newUser = new User(req.body); // Creo un nuevo usuario y le asigno el body del request
    await newUser.save(); // guardo en la bd
    return res.status(400).json(newUser); // devuelvo el status y la info del nuevo usuario
}
// Loguearse
export const signIn = async ( req: Request, res: Response ) => {
    if (!req.body.email || !req.body.password) { // Verifico si el usuario me manda email y password
        return res.status(400).json({msg: 'Debe Ingresar su email y password'});
    }
    const user = await User.findOne({email: req.body.email}); // Busco en la bd si ya esta registrado el email
    if (!user) {
        return res.status(400).json({msg: 'El usuario no existe'});
    }
    const isMatch = user.comparePassword(req.body.password); // comparo el password ingresado 
    if (isMatch) {
        return res.status(200).json({token: createToken(user)}); // devuelvo el token 
    }
    return res.status(400).json({ // si el password es incorrecto devuelvo un msje de error
        msg: 'Email o password incorrecto'
    });
}